//
// Created by andrey on 06.12.23.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_INFO_H
#define IMAGE_TRANSFORMER_IMAGE_INFO_H

#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t components[3];
};

struct image {
    uint32_t width;
    uint32_t height;
    struct pixel* data;
};

struct image create_image(uint16_t width, uint16_t height);
void free_image(struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_INFO_H

