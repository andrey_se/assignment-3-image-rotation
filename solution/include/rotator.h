//
// Created by andrey on 10.12.23.
//

#ifndef IMAGE_TRANSFORMER_ROTATOR_H
#define IMAGE_TRANSFORMER_ROTATOR_H

#include "image_info.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>

enum rotation_result {
    ROTATION_OK,
    ROTATION_ERROR
};

bool is_valid_angle(const char *angle_str, int* angle);
struct image rotate(const struct image *input, int rotations, bool *flag);

#endif //IMAGE_TRANSFORMER_ROTATOR_H
