//
// Created by andrey on 10.12.23.
//

#ifndef IMAGE_TRANSFORMER_RAW_BMP_INFO_H
#define IMAGE_TRANSFORMER_RAW_BMP_INFO_H

#include "image_info.h"
#include <bits/types/FILE.h>
#include <stdint.h>

#define BFtype 0x4d42;
#define BFreserved 0;
#define BIplanes 1;
#define BIsize 40;
#define BBcount 24;
#define BIX 2834;
#define BIY 2834;
#define BIclr 0;


#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
};

enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);
long calculator_padding(uint32_t width);

#endif // IMAGE_TRANSFORMER_RAW_BMP_INFO_H
