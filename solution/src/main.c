#include "image_info.h"
#include "raw_bmp_info.h"
#include "rotator.h"
#include "main.h"

int main(int argc, char *argv[]) {

    // проверка на количество аргументов
    if (argc != 4) {
        fprintf(stderr, "Проверьте количество аргументов: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // открытие исходного изображения
    struct image source_img;
    FILE *source_file = fopen(argv[1], READMODE);
    if (!source_file) {
        fprintf(stderr, "Не удалось открыть файл\n");
        exit(EXIT_FAILURE);
    }

    // извлечение данных из файла в структуру
    enum read_status read_result = from_bmp(source_file, &source_img);
    fclose(source_file);

    // проверка на успех чтения исходного изображения
    if (read_result != READ_OK) {
        free_image(&source_img);
        fprintf(stderr, "Произошла ошибка чтения исходного файла: %d\n", read_result);
        exit(EXIT_FAILURE);
    }

    int angle;
    if (!is_valid_angle(argv[3], &angle)) {
        fprintf(stderr, "Введённый угол не совпадает с возможными (0, 90, -90, 180, -180, 270, -270)\n");
        exit(EXIT_FAILURE);
    }

    // поворот
    int rotations = (4 - angle / 90) % 4;
    bool flag;
    struct image transformed_img = rotate(&source_img, rotations, &flag);
    free_image(&source_img);

    // проверка на успех
    if (flag == false) {
        free_image(&source_img);
        free_image(&transformed_img);
        exit(EXIT_FAILURE);
    }

    // проверка на успех чтения файла результата
    FILE *transformed_file = fopen(argv[2], WRITEMODE);
    if (!transformed_file) {
        free_image(&source_img);
        free_image(&transformed_img);
        exit(EXIT_FAILURE);
    }

    // запись в файл
    enum write_status write_result = to_bmp(transformed_file, &transformed_img);
    fclose(transformed_file);

    // проверка на успех записи
    if (write_result != WRITE_OK) {
        free_image(&source_img);
        free_image(&transformed_img);
        exit(EXIT_FAILURE);
    }

    free_image(&source_img);

    exit(EXIT_SUCCESS);
}
