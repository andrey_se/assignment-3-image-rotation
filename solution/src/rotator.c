//
// Created by andrey on 10.12.23.
//

#include "rotator.h"

bool is_valid_angle(const char *angle_str, int *angle) {
    char *endptr;
    int angle_value = (int) strtol(angle_str, &endptr, 10);

    if (*endptr != '\0' && *endptr != '\n') {
        return false;
    }

    angle_value %= 360;
    int unique_angles[] = {0, 90, -90, 180, -180, 270, -270};

    for (int i = 0; i < 7; i++) {
        if (angle_value == unique_angles[i]) {
            *angle = angle_value;
            return true;
        }
    }
    return false;
}


struct image rotate(const struct image *input, int rotations, bool *flag) {
    *flag = true;
    if (rotations == 0) {
        struct image new_image = create_image(input->width, input->height);
        for (uint64_t i = 0; i < input->width * input->height; i++) {
            new_image.data[i] = input->data[i];
        }
        return new_image;
    } else if (rotations == 1) {
        struct image new_image = create_image(input->height, input->width);
        for (uint64_t row = 0; row < input->width; row++) {
            for (uint64_t cell = 0; cell < input->height; cell++) {
                new_image.data[input->height * row + cell] = input->data[input->width * (input->height - cell - 1) +
                                                                         row];
            }
        }
        return new_image;
    } else if (rotations == 2) {
        struct image new_image = create_image(input->width, input->height);
        for (uint64_t row = 0; row < input->height; row++) {
            for (uint64_t cell = 0; cell < input->width; cell++)
                new_image.data[input->width * (input->height - row - 1) + (input->width - cell - 1)] = input->data[
                        input->width * row + cell];
        }
        return new_image;
    } else if (rotations == 3) {
        struct image new_image = create_image(input->height, input->width);

        for (uint64_t row = 0; row < input->width; row++) {
            for (uint64_t cell = 0; cell < input->height; cell++) {
                new_image.data[input->height * row + cell] = input->data[input->width * cell +
                                                                         (input->width - row - 1)];
            }
        }

        return new_image;
    }
    *flag = false;
    return create_image(0, 0);
}