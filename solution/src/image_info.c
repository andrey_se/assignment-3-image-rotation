//
// Created by andrey on 06.12.23.
//

#include "image_info.h"

struct image create_image(uint16_t width, uint16_t height) {
    struct image img = {.width = 0, .height = 0, .data = NULL};
    struct pixel* data = malloc(width * height * sizeof(struct pixel));

    img.data = data;
    if (!img.data) {
        return img;
    }

    img.width = width;
    img.height = height;

    return img;
}

void free_image(struct image *img) {
    if (img) {
        if (img->data) {
            free(img->data);
        }
        img->width = 0;
        img->height = 0;
        img->data = NULL;
    }
}
