#include "raw_bmp_info.h"

long calculator_padding(uint32_t width) {
    return (4 - (long) (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img || fseek(in, 0, SEEK_END) != 0) {
        return READ_INVALID_HEADER;
    }

    long file_size = ftell(in);
    if (file_size == -1L) {
        return READ_INVALID_HEADER;
    }

    rewind(in);

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.bfileSize != file_size) {
        return READ_INVALID_BITS;
    }

    if (header.bOffBits + header.biSizeImage > file_size) {
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = abs((int) header.biHeight);
    img->data = (struct pixel *) malloc(sizeof(struct pixel) * img->width * img->height);
    if (!img->data) {
        return READ_INVALID_BITS;
    }

    long padding = calculator_padding(header.biWidth);

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        free_image(img);
        return READ_INVALID_BITS;
    }

    for (uint32_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    const uint8_t padding = (4 - (long) (img->width * sizeof(struct pixel)) % 4) % 4;

    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) + (img->width * sizeof(struct pixel) + sizeof(padding)) * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biSizeImage = (img->width * sizeof(struct pixel) + sizeof(padding)) * img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (!fwrite(&header, sizeof(header), 1, out)) {
        return WRITE_ERROR;
    }
    size_t current_pos = 0;
    for (uint32_t line_number = 0; line_number < img->height; line_number++) {
        if (fwrite(img->data + current_pos, img->width * sizeof (struct pixel), 1, out) != 1
            || fseek(out, padding, SEEK_CUR)) {
            return WRITE_ERROR;
        }
        current_pos += img->width;
    }

    return WRITE_OK;
}
